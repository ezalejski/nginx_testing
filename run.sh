#!/usr/bin/env bash

set -eu

DOCKER_REPO='ezalejski'
DOCKER_NAME='nginx_testing'
DOCKER_TAG=${1:-"latest"}
DOCKER_IMAGE="$DOCKER_REPO/$DOCKER_NAME"

DOCKER=$(command -v docker) || { echo "docker is not available!"; exit 1; }

${DOCKER} stop "$DOCKER_NAME" || true
${DOCKER} run --rm --name "$DOCKER_NAME" -p 8080:80 "${DOCKER_IMAGE}:${DOCKER_TAG}"
